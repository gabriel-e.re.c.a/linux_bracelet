/*
 * Copyright (C) 2016 Freescale Semiconductor, Inc. All Rights Reserved.
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2 of the License, or
 * (at your option) any later version.

 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.

 * You should have received a copy of the GNU General Public License along
 * with this program; if not, write to the Free Software Foundation, Inc.,
 * 51 Franklin Street, Fifth Floor, Boston, MA 02110-1301 USA.
 */

#include <linux/types.h>
#include <linux/init.h>
#include <linux/delay.h>
#include <linux/err.h>
#include <linux/io.h>

#include "../mxsfb.h"

static struct fb_videomode st7789s_lcd_modedb[] = {
	{
	 "ST7789S-QVGA", 60, 320, 240, 200000,
	 0, 0,
	 0, 0,
	 0, 0,
	 0,
	 FB_VMODE_NONINTERLACED,
	 0,
	},
};

static struct mpu_lcd_config lcd_config = {
	.bus_mode = MPU_BUS_8080,
	.interface_width = 16,
	.panel_bpp = 16,
};
void mpu_st7789s_get_lcd_videomode(struct fb_videomode **mode, int *size,
		struct mpu_lcd_config **data)
{
	*mode = &st7789s_lcd_modedb[0];
	*size = ARRAY_SIZE(st7789s_lcd_modedb);
	*data = &lcd_config;
}

int mpu_st7789s_lcd_setup(struct mxsfb_info * mxsfb)
{
	if (mxsfb == NULL)
		return -1;

	/* Sleep out */
	mxsfb_mpu_access(mxsfb, MPU_CMD, MPU_WRITE, 0x11);

	/* Analog setting */ 
	mxsfb_mpu_access(mxsfb, MPU_CMD, MPU_WRITE, 0xB2);
	mxsfb_mpu_access(mxsfb, MPU_DATA, MPU_WRITE, 0x0C);
	mxsfb_mpu_access(mxsfb, MPU_DATA, MPU_WRITE, 0x0C);
	mxsfb_mpu_access(mxsfb, MPU_DATA, MPU_WRITE, 0x00);
	mxsfb_mpu_access(mxsfb, MPU_DATA, MPU_WRITE, 0x33);
	mxsfb_mpu_access(mxsfb, MPU_DATA, MPU_WRITE, 0x33);

	mxsfb_mpu_access(mxsfb, MPU_CMD, MPU_WRITE, 0xB7);
	mxsfb_mpu_access(mxsfb, MPU_DATA, MPU_WRITE, 0x35);

	mxsfb_mpu_access(mxsfb, MPU_CMD, MPU_WRITE, 0xBB);	
    mxsfb_mpu_access(mxsfb, MPU_DATA, MPU_WRITE, 0x2B);

	mxsfb_mpu_access(mxsfb, MPU_CMD, MPU_WRITE, 0xC0);
	mxsfb_mpu_access(mxsfb, MPU_DATA, MPU_WRITE, 0x2C);

	mxsfb_mpu_access(mxsfb, MPU_CMD, MPU_WRITE, 0xC2);
	mxsfb_mpu_access(mxsfb, MPU_DATA, MPU_WRITE, 0x01);
    mxsfb_mpu_access(mxsfb, MPU_DATA, MPU_WRITE, 0xFF);

	mxsfb_mpu_access(mxsfb, MPU_CMD, MPU_WRITE, 0xC3);
    mxsfb_mpu_access(mxsfb, MPU_DATA, MPU_WRITE, 0x11);

	mxsfb_mpu_access(mxsfb, MPU_CMD, MPU_WRITE, 0xC4);
	mxsfb_mpu_access(mxsfb, MPU_DATA, MPU_WRITE, 0x20);

	mxsfb_mpu_access(mxsfb, MPU_CMD, MPU_WRITE, 0xC6);
    mxsfb_mpu_access(mxsfb, MPU_DATA, MPU_WRITE, 0x0F);

	mxsfb_mpu_access(mxsfb, MPU_CMD, MPU_WRITE, 0xD0);
	mxsfb_mpu_access(mxsfb, MPU_DATA, MPU_WRITE, 0xA4);
    mxsfb_mpu_access(mxsfb, MPU_DATA, MPU_WRITE, 0xA1);
	                 
	/* Gamma setting */ 
	mxsfb_mpu_access(mxsfb, MPU_CMD, MPU_WRITE, 0xE0);
    mxsfb_mpu_access(mxsfb, MPU_DATA, MPU_WRITE, 0xD0);
    mxsfb_mpu_access(mxsfb, MPU_DATA, MPU_WRITE, 0x00);
    mxsfb_mpu_access(mxsfb, MPU_DATA, MPU_WRITE, 0x05);
    mxsfb_mpu_access(mxsfb, MPU_DATA, MPU_WRITE, 0x0E);
    mxsfb_mpu_access(mxsfb, MPU_DATA, MPU_WRITE, 0x15);
    mxsfb_mpu_access(mxsfb, MPU_DATA, MPU_WRITE, 0x0D);
    mxsfb_mpu_access(mxsfb, MPU_DATA, MPU_WRITE, 0x37);
    mxsfb_mpu_access(mxsfb, MPU_DATA, MPU_WRITE, 0x43);
    mxsfb_mpu_access(mxsfb, MPU_DATA, MPU_WRITE, 0x47);
    mxsfb_mpu_access(mxsfb, MPU_DATA, MPU_WRITE, 0x09);
    mxsfb_mpu_access(mxsfb, MPU_DATA, MPU_WRITE, 0x15);
    mxsfb_mpu_access(mxsfb, MPU_DATA, MPU_WRITE, 0x12);
    mxsfb_mpu_access(mxsfb, MPU_DATA, MPU_WRITE, 0x16);
    mxsfb_mpu_access(mxsfb, MPU_DATA, MPU_WRITE, 0x19);


	mxsfb_mpu_access(mxsfb, MPU_CMD, MPU_WRITE, 0xE1);
    mxsfb_mpu_access(mxsfb, MPU_DATA, MPU_WRITE, 0xD0);
    mxsfb_mpu_access(mxsfb, MPU_DATA, MPU_WRITE, 0x00);
    mxsfb_mpu_access(mxsfb, MPU_DATA, MPU_WRITE, 0x05);
    mxsfb_mpu_access(mxsfb, MPU_DATA, MPU_WRITE, 0x0D);
    mxsfb_mpu_access(mxsfb, MPU_DATA, MPU_WRITE, 0x0C);
    mxsfb_mpu_access(mxsfb, MPU_DATA, MPU_WRITE, 0x06);
    mxsfb_mpu_access(mxsfb, MPU_DATA, MPU_WRITE, 0x2D);
    mxsfb_mpu_access(mxsfb, MPU_DATA, MPU_WRITE, 0x44);
    mxsfb_mpu_access(mxsfb, MPU_DATA, MPU_WRITE, 0x40);
    mxsfb_mpu_access(mxsfb, MPU_DATA, MPU_WRITE, 0x0E);
    mxsfb_mpu_access(mxsfb, MPU_DATA, MPU_WRITE, 0x1C);
    mxsfb_mpu_access(mxsfb, MPU_DATA, MPU_WRITE, 0x18);
    mxsfb_mpu_access(mxsfb, MPU_DATA, MPU_WRITE, 0x16);
    mxsfb_mpu_access(mxsfb, MPU_DATA, MPU_WRITE, 0x19);


	mxsfb_mpu_access(mxsfb, MPU_CMD, MPU_WRITE, 0x36);
    //mxsfb_mpu_access(mxsfb, MPU_DATA, MPU_WRITE, 0x00); //0° 
    mxsfb_mpu_access(mxsfb, MPU_DATA, MPU_WRITE, 0xA0); //90°
    //mxsfb_mpu_access(mxsfb, MPU_DATA, MPU_WRITE, 0xC0); //180°
	//mxsfb_mpu_access(mxsfb, MPU_DATA, MPU_WRITE, 0x60); //270°

    mxsfb_mpu_access(mxsfb, MPU_CMD, MPU_WRITE, 0x3A);
	if (mxsfb->mpu_lcd_sigs->panel_bpp == 16)
		mxsfb_mpu_access(mxsfb, MPU_DATA, MPU_WRITE, 0x55);
	else
		mxsfb_mpu_access(mxsfb, MPU_DATA, MPU_WRITE, 0x66);  /* 18 bpp */

	/* Set row address */
	mxsfb_mpu_access(mxsfb, MPU_CMD, MPU_WRITE, 0x2A);
	mxsfb_mpu_access(mxsfb, MPU_DATA, MPU_WRITE, 0x00);
	mxsfb_mpu_access(mxsfb, MPU_DATA, MPU_WRITE, 0x00);
	mxsfb_mpu_access(mxsfb, MPU_DATA, MPU_WRITE, (((st7789s_lcd_modedb[0].xres - 1) >> 8) & 0xFF));
	mxsfb_mpu_access(mxsfb, MPU_DATA, MPU_WRITE, ((st7789s_lcd_modedb[0].xres - 1) & 0xFF)); 

	/* Set column address */
	mxsfb_mpu_access(mxsfb, MPU_CMD, MPU_WRITE, 0x2B);
	mxsfb_mpu_access(mxsfb, MPU_DATA, MPU_WRITE, 0x00);
	mxsfb_mpu_access(mxsfb, MPU_DATA, MPU_WRITE, 0x00);
	mxsfb_mpu_access(mxsfb, MPU_DATA, MPU_WRITE, (((st7789s_lcd_modedb[0].yres - 1) >> 8) & 0xFF));
	mxsfb_mpu_access(mxsfb, MPU_DATA, MPU_WRITE, ((st7789s_lcd_modedb[0].yres - 1) & 0xFF));   

	/* Display on */
	mxsfb_mpu_access(mxsfb, MPU_CMD, MPU_WRITE, 0x29);

	/* Memory write */
	mxsfb_mpu_access(mxsfb, MPU_CMD, MPU_WRITE, 0x2C);

	return 0;
}

int mpu_st7789s_lcd_poweroff(struct mxsfb_info * mxsfb)
{
	/* Display off */
	mxsfb_mpu_access(mxsfb, MPU_CMD, MPU_WRITE, 0x28);

	/* Sleep in */
	mxsfb_mpu_access(mxsfb, MPU_CMD, MPU_WRITE, 0x10);

	return 0;
}
